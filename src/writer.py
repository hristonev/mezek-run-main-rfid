import time
from threading import Thread
from queue import Empty, Queue
from serial import Serial


class Writer(Thread):
    def __init__(self, config: dict, serial: Serial, bus: Queue):
        self.config = config
        self.bus = bus
        self.serial = serial
        super().__init__()

    def run(self):
        while True:
            try:
                command = self.bus.get(timeout=.001)
                cmd_bytes = bytearray.fromhex(command)
                for cmd_byte in cmd_bytes:
                    hex_byte = ("{0:02x}".format(cmd_byte))
                    self.serial.write(bytearray.fromhex(hex_byte))
                time.sleep(1 / self.config['com']['read_rate'])
            except Empty:
                pass
