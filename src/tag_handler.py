from threading import Thread
from queue import Empty, Queue
import re
from os import environ
import base64
from datetime import datetime
import hmac
import hashlib
import requests
import logging


class TagHandler(Thread):
    def __init__(self, bus: Queue, config: dict):
        self.bus = bus
        self.known = set()
        self.hmac_secret = base64.b64decode(environ.get('HMAC_SECRET'))
        self.config = config.get('server')
        self.logger = logging.getLogger()
        super().__init__()

    def run(self):
        while True:
            try:
                data = self.bus.get(timeout=.1)
                # E2 00 00 20 15 05 01 15 02 30 58 51
                res = re.match("^A0 13.*(E2 00 00.{27}).*", data)
                if res:
                    tag = res.groups()[0].replace(' ', '')
                    if tag not in self.known:
                        self.logger.info(f"Read new tag {tag}")
                        self.send_to_server(tag)
                        self.known.add(tag)

            except Empty:
                pass

    def send_to_server(self, tag):
        new_tag_config = self.config.get('new_tag')
        base = self.config.get('base')
        endpoint = new_tag_config.get('endpoint')
        method = new_tag_config.get('method')
        date, signature = self.hmac_sign(
            method=method,
            endpoint=endpoint
        )
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': f'hmac-rfid algorithm="sha512", headers="Date", signature="{signature}"',
            'Date': date
        }
        resp = requests.request(
            method=new_tag_config.get('method'),
            url=f'{base}{endpoint}',
            data={"tag": tag},
            headers=headers,
            verify=False,
        )
        print(resp.content)

    def hmac_sign(self, method, endpoint):
        date = datetime.now()
        data = f"date: {date}\n{method} {endpoint}"
        signature = hmac.new(
            self.hmac_secret,
            data.encode(),
            hashlib.sha512
        ).hexdigest()
        return f'{date}', signature
