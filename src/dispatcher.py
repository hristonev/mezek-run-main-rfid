from threading import Thread
import logging
import serial
from src.writer import Writer
from src.reader import Reader
from src.tag_handler import TagHandler
from queue import Queue, Empty
import time


class Dispatcher(Thread):
    def __init__(self, config: dict, arg):
        self.arg = arg
        self.config = config
        self.logger = logging.getLogger()
        self.command_bus = Queue()
        self.data_bus = Queue()
        self.data_block_bus = Queue()
        self.data = []
        try:
            self.serial = serial.Serial(
                port=self.arg.port,
                baudrate=self.config['com']['baud_rate'],
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS
            )
        except OSError:
            self.logger.error("Unable to open serial port")
            exit(1)
        super().__init__()

    def run(self):
        if not self.serial.is_open:
            self.logger.error('Could not open serial port!')
        self.logger.info(f'Port {self.arg.port} is opened')
        workers = [
            Writer(config=self.config, serial=self.serial, bus=self.command_bus),
            Reader(serial=self.serial, bus=self.data_bus),
            TagHandler(bus=self.data_block_bus, config=self.config)
        ]
        for w in workers:
            w.start()

        self.init()
        while True:
            self.process_data()
            self.read_data()

        for w in workers:
            w.join()
        while True:
            pass

    def read_data(self):
        if self.command_bus.empty():
            self.command_bus.put('A0 06 01 8B 01 00 01 CC')

    def process_data(self):
        try:
            bit = self.data_bus.get(timeout=.001)
            self.data.append(bit.decode())
        except Empty:
            if len(self.data) > 0:
                data = ' '.join(self.data).upper()
                self.data_block_bus.put(data)
                if data != 'A0 0A 01 8B 00 00 00 00 00 00 00 CA':
                    self.logger.info(f'Block: {data}')
            self.data = []
            pass

    def init(self):
        self.command_bus.put('A0 03 FF 79 E5')
