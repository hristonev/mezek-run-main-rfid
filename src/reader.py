from threading import Thread
from queue import Queue
from serial import Serial
import binascii


class Reader(Thread):
    def __init__(self, serial: Serial, bus: Queue):
        self.serial = serial
        self.bus = bus
        super().__init__()

    def run(self):
        while True:
            self.bus.put(binascii.hexlify(bytearray(self.serial.read())))
