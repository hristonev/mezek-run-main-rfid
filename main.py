import yaml
import argparse
import logging
from logging.handlers import TimedRotatingFileHandler
from src.dispatcher import Dispatcher
from os import environ

# Config
with open('config.yaml', 'rt') as fp:
    config = yaml.safe_load(fp)

# UI
parser = argparse.ArgumentParser()
parser.add_argument('--port', required=True, help='Serial port system address like /dev/ttyUSB0')
arg = parser.parse_args()

# ENV
if not environ.get('HMAC_SECRET'):
    raise Exception('Environment variables HMAC secret not set')

# Logger config
logging.basicConfig(level=logging.DEBUG)
handler = TimedRotatingFileHandler(filename=config['logger']['name'], when='H')
formatter = logging.Formatter(config['logger']['format'])
handler.setFormatter(formatter)
logger = logging.getLogger()
for h in logger.handlers:
    logger.removeHandler(h)
logger.addHandler(handler)

if __name__ == '__main__':
    t = [
        Dispatcher(config, arg)
    ]
    try:
        for i in t:
            i.start()
    except RuntimeError:
        print('Dispatcher start exception!')
        pass
